// Coded by ScratchyCode
// Compile in gcc with option -lm
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int even(unsigned long long int num);
int prime(unsigned long long int num, unsigned long long int vect[], unsigned long long int index);

int main(){
    unsigned long long int i, j=0, dim, index, num;
    unsigned long long int *vect;
    char file[] = "Primes.txt";
    
    printf("\nEnter a maximum number: ");
    scanf("%llu",&dim);
    
    // array length estimated with the prime number theorem + Riemann hypothesis (+100 for safe memory management)
    unsigned long long int len = (unsigned long long int)(100 + ((long double)(dim/log((long double)dim)) + 
    sqrt((long double)dim)*log((long double)dim)));
    vect = malloc(len * sizeof(unsigned long long int));
    if(vect == NULL){
        perror("\nError");
        exit(1);
    }
    
    // initialize the vector with the known prime numbers
    vect[0] = 1;
    vect[1] = 2;
    vect[2] = 3;
    vect[3] = 5;
    
    // index of the array set to the last known element
    index = 3;
    
    // calculation from the last known number up to entered SUP
    for(num=6; num<=dim; num++){
        // exclude even numbers from test
        if(even(num)){
            j++;
            continue;
        }
        
        /*
        // extend the buffer size if it has been underestimated
        if(index == len){
            printf("\nMemory realloc...\n");
            vect = realloc(vect,(len+100) * sizeof(unsigned long long int));
            len += 100;
            if(vect == NULL){
                perror("\nError");
                exit(1);
            }
        }
        */
        
        // primality check
        if(prime(num,vect,index)){
            index++;
            vect[index] = num;
        }
        
        j++; // for percentage
        printf("\rCalculating:\t\t%0d%%",(int)(j*100./dim));
        fflush(stdout);
    }
    
    printf("\nCompleted:\t\t100%\n");
    
    // writing result
    FILE *pf = fopen(file,"w");
    if(pf == NULL){
        perror("\nError");
        exit(1);
    }

    for(i=0; i<=index; i++){
        fprintf(pf,"%llu\n",vect[i]);
    }

    fclose(pf);
    free(vect);

    return 0;
}

int even(unsigned long long int num){
    if(num%2 == 0){
        return 1;
    }else{
        return 0;
    }
}

int prime(unsigned long long int num, unsigned long long int vect[], unsigned long long int index){
    unsigned long long int i, squareRoot=(unsigned long long int)(sqrtl((long double)index+1));
    
    for(i=2; i<=squareRoot; i++){
        // if num is divisible by a prime number then num is not a prime number
        if(num % vect[i] == 0){
            return 0;
        }
    }
    
    return 1;
}
