# Prime-numbers

Different ways to generate prime numbers.

# Descriptions

Prime_threads.c

This program performs the Primes optimizations, but split the calculation in half on two threads.

PrimesVect.c

An N number is prime if it is not divided by any prime number less than N.
This program performs the least number of primality checks possible.

Primes.c

A number is prime if it is not divisible by any smaller number.
Calculate prime numbers with some optimizations up to the set limit.

Wilson-formula

Application of Wilson's Theorem for the search for prime numbers.
