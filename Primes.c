// Coded by ScratchyCode
// Calculate prime numbers with some optimizations up to the set limit.
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int test(unsigned long long int num);

int main(){
    unsigned long long int i, j=0, lim;
    char log[] = "Primes.txt";

    printf("\nEnter a maximum number: ");
    scanf("%llu",&lim);

    // writing to a file instead of a screen optimizes the times
    FILE *fp;
    fp = fopen(log,"w");
    if(fp == NULL){
        perror("\nError");
        exit(1);
    }

    fprintf(fp,"1\n2\n3\n");
    fflush(fp);
    
    // perform the primality checks
    for(i=3; i<=lim; i++){
        if(i%2 == 0){
            // exclusion of even numbers
            j++;
            continue;
        }else{
            if(test(i)){
                fprintf(fp,"%llu\n",i);
            }
        }
        
        j++; // for percentage
        printf("\rCalculating:\t\t%0d%%",(int)(j*100./lim));
        fflush(stdout);
    }
    
    fclose(fp);
    
    printf("\nCompleted:\t\t100%\n");

    return 0;
}

int test(unsigned long long int num){
    unsigned long long int i;
    unsigned long long int lim;

    // makes sense control the primality to the square root of the number
    lim = (unsigned long long int)(sqrtl((long double)num));
    
    // if tmp is a generic natural number -> i is an odd number
    unsigned long long int tmp = 1;
    do{
        // control the division rest only for odd numbers
        i = 2*tmp+1;
        if(num%i == 0){
            return 0;
        }
        tmp++;
    }while(i <= lim);
    
    return 1;
}
