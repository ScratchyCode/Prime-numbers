// Coded by ScratchyCode
// Compile with -pthread
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>

struct structure{
    unsigned long long int dim;
    unsigned long long int lim;
    unsigned long long int *vect2;
    unsigned long long int index;
};

void *parallel_main(void *parameters);
int test(unsigned long long int num);

int main(){
    unsigned long long int i, lim, index=0;
    unsigned long long int *vect1, *vect2;
    char log[] = "Primes.txt";
    pthread_t thread;
    
    printf("\nEnter the maximum number of the search: ");
    scanf("%llu",&lim);
    
    unsigned long long int dim = (unsigned long long int)(lim/2.);
    vect1 = malloc(dim * sizeof(unsigned long long int));
    vect2 = malloc(dim * sizeof(unsigned long long int));
    if(vect1 == NULL || vect2 == NULL){
        perror("\nError");
        exit(1);
    }
    
    printf("\n\aCalculation in progress, please wait...\n");
    
    // data to be passed to thread saved into structure
	struct structure thread_args;
	thread_args.dim = dim+1;
	thread_args.lim = lim;
	thread_args.vect2 = vect2;
    
    // create parallel execution to the main
	pthread_create(&thread,NULL,parallel_main,&thread_args);
    
    // primality test
    for(i=3; i<=dim; i++){
	    if(i%2 == 0){
	        // exclusion of even numbers
            continue;
        }else{
            if(test(i)){
                vect1[index] = i;
                index++;
            }
        }
    }
    
    // wainting thread
    pthread_join(thread,NULL);
    
    printf("Search completed.\n");
    
    FILE *fp;
    fp = fopen(log,"w");
    if(fp == NULL){
	    perror("\nError");
        exit(1);
    }

    fprintf(fp,"1\n2\n3\n");
    
    for(i=0; i<index; i++){
        fprintf(fp,"%llu\n",vect1[i]);
    }
    
    index = thread_args.index;
    for(i=0; i<index; i++){
        fprintf(fp,"%llu\n",vect2[i]);
    }
    
    fclose(fp);
    free(vect1);
    free(vect2);

    return 0;
}

void *parallel_main(void *parameters){
    unsigned long long int i, num, lim, index=0, *vect2;
    struct structure *data = (struct structure *) parameters;
    
    num = data->dim;
    lim = data->lim;
    vect2 = data->vect2;
    
    // primality test
    for(i=num; i<=lim; i++){
	    if(i%2 == 0){
	        // exclusion of even numbers
            continue;
        }else{
            if(test(i)){
                vect2[index] = i;
                index++;
            }
        }
    }
    
    data->index = index;
	
	return NULL;
}

int test(unsigned long long int num){
    unsigned long long int i;
    unsigned long long int lim;

    // the primality check is performed up to the square root of the number to be checked
    lim = (unsigned long long int)(sqrtl((long double)num));
    
    // tmp it is a natural number --> i it's an odd number
    unsigned long long int tmp = 1;
    do{
        // check the division only for odd numbers
        i = 2*tmp+1;
        if(num%i == 0){
            return 0;
        }
        tmp++;
    }while(i <= lim);
    
    return 1;
}
