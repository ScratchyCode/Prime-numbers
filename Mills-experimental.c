#include <stdio.h>
#include <math.h>

int main(){
    unsigned long long int i, lim, m;
    long double n;
    const long double theta = 1.30637788386308069;
    
    printf("\nEnter the maximum number of the search (<10): ");
    scanf("%llu",&lim);
    
    for(i=1; i<=lim; i++){
        // Mills' theorem
        m = pow(3,i);
        n = pow(theta,m);
        printf("n = %llu\t=>\tf(n) = %Lf\n",i,n);
    }
    
    return 0;
}
