// Coded by ScratchyCode
#include <stdio.h>
#include <stdlib.h>

unsigned long long int factorial(unsigned long long int x);

int main(){
	unsigned long long int i, var, lim;

	printf("Enter the max value to search: ");
	scanf("%llu", &lim);
	if(lim > 30 || lim <= 0){
	    printf("\nError.\n");
	    exit(1);
	}
	printf("\n");
	
	// calcoli
	for(i=1; i<=lim; i++){
		var = factorial(i-1) + 1;
		if(var%i == 0){
		    printf("%lld\n", i);
		}
	}
	
	printf("\nEnd.\n");
	
	return 0;
}

unsigned long long int factorial(unsigned long long int x){
	unsigned long long int i, factorial = 1;

	for(i=1; i<=x; i++){
		factorial = factorial*i;
	}

	return factorial;
}
